FROM osrf/ros:noetic-desktop-full

SHELL ["/bin/bash", "-c"]

RUN apt-get update && apt-get upgrade -y && \
    apt-get install -y --no-install-recommends software-properties-common git nano python3-pip python3-catkin-tools qtbase5-dev libqt5svg5-dev libzmq3-dev libdw-dev \
    ros-noetic-gazebo-ros-pkgs ros-noetic-gazebo-ros-control

WORKDIR /catkin_ws

COPY . src/

RUN cd src/drone/scripts/DJITelloPy && \
    pip install -e .

RUN git clone https://github.com/BehaviorTree/Groot.git src/Groot

RUN rosdep install --from-paths src --ignore-src -r -y && \
    source /opt/ros/noetic/setup.bash && \
    catkin_make

COPY ./hector_quadrotor/models /root/.gazebo/models

RUN echo "source /opt/ros/noetic/setup.bash" >> ~/.bashrc
RUN echo "source /catkin_ws/devel/setup.bash" >> ~/.bashrc

RUN echo "alias sb='source ~/.bashrc'" >> ~/.bashrc
RUN echo "alias init='rosservice call /init \"{}\"'" >> ~/.bashrc
RUN echo "alias takeoff='rosservice call /takeoff_land \"{command: 'takeoff'}\"'" >> ~/.bashrc
RUN echo "alias land='rosservice call /takeoff_land \"{command: 'land'}\"'" >> ~/.bashrc
RUN echo "alias move_x='rosservice call /move \"{x: 20.0, y: 0.0, z: 0.0, speed: 5.0, rotate: 0}\"'" >> ~/.bashrc

COPY ./ros_entrypoint.sh /ros_entrypoint.sh
RUN chmod +x /ros_entrypoint.sh

ENTRYPOINT ["/ros_entrypoint.sh"]
CMD ["bash"]