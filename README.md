# PPMROB
## Interfaces
*by Nino Wegleitner, 01644401*

Used to define custom messages, services and actions.

The definitions can be found inside the [interfaces](/interfaces) folder.

## Drone Node
*by Nino Wegleitner, 01644401*

ROS wrapper for Tello (https://djitellopy.readthedocs.io/en/latest/tello/). Publishes IMU (*/imu*), battery (*/battery*) and video stream (*/image*). Use service */takeoff_land* for takeoff and landing, service */init* for initialization of tello drone and */move* to update move commands. 

Publisher: 
* */imu* (type: sensor_msgs/Imu)
* */battery* (type: std_msgs/Int16)
* */image* (type: sensor_msgs/Image)

Service (Server): 
* */init* (type: interfaces/Init)
* */move* (type: interfaces/Move)
* */takeoff_land* (type: interfaces/TakeOffLand)

There are two bag files with recordings of */image*, */imu* and */battery* topics. Use `rosbag play -l drone/bags/*.bag` to replay the topics in ROS. This can be used for testing other nodes e.g. CV.

To start the drone node, first build the required containers using docker build

```
docker build -t ppmrob:latest -f Dockerfile .
```

And then start the drone container and RViz using docker compose

```
xhost +local:docker
```

```
docker compose -f compose.drone.yml up
```

The implementation of the wrapper can be found here [here](/drone/scripts/drone.py).

## Drone Simulator (Gazebo)
*by Nino Wegleitner, 01644401*

![Gazebo](hector_quadrotor/gazebo.png)

Gazebo simulation of a quadrotor drone using *hector_quadrotor* (https://github.com/tu-darmstadt-ros-pkg/hector_quadrotor). Same interfaces as the real tello drone. The only difference is that the control commands in the */move* service are defined with regards to the *map* coordinate frame and the the *base_link* of the drone. 

Publisher: 
* */raw_imu* (type: sensor_msgs/Imu)
* */battery* (type: std_msgs/Int16)
* */image* (type: sensor_msgs/Image)

Service (Server): 
* */init* (type: interfaces/Init)
* */move* (type: interfaces/Move)
* */takeoff_land* (type: interfaces/TakeOffLand)

To start the simulator node, first build the required containers using docker build

```
docker build -t ppmrob:latest -f Dockerfile .
```

And then start the gazebo container and RViz using docker compose

```
xhost +local:docker
```

```
docker compose -f compose.gazebo.yml up
```

The implementation of the hector_quadrotor wrapper can be found here [here](/drone/scripts/drone_sim.py). The custom world is defined [here](hector_quadrotor/hector_quadrotor_gazebo/world/victim_world) and the ground image [here](hector_quadrotor/models/my_ground_plane/materials/textures/Floor.png). 

## Computer Vision Node
Python ROS node
E.g. YOLOv5 transfer learning: https://github.com/ultralytics/yolov5/issues/1314 
Runs inference on input Images and outputs detections
Runs also classic CV algorithms to detect (border) lines.

Subscriber: 
* */image* (type: sensor_msgs/Image)

Publisher: 
* */detections* (type: interfaces/Detections)

### Computer vision node implementation
*by Thomas Brezina, 11778916*

1. developed docker image to use torch and cv2 in vision node
2. in computer vision node used torch lib to load the yolo5 model with the best weights for the given human silhouette
3. using cv_bridge from ros to convert image message to a cv2 image
4. using cv2 to transfer the image from rgb8 encoding to greyscale
5. applying the loaded yolo5 model on the image and create detection array - empty if no detections found
6. from the detection array i create a Pose2D object (using the center of the detection) and append it to the Detection object and publish it as a Detection message to the /detections topic
7. calculating and logging performance of processing and detecting a human silhouette from a image
8. invented threading - but no real performance difference

build the docker image with latest changes on computer_vision.py:
* ```docker build -t ppmrob-vision:latest -f Dockerfile_torch .```
* add ```--no-cache``` if changes made on the repo's main branch
* or make changes in the file in the container itself 
* try ```docker system prune -a``` if docker image build throws any error

start the container via docker compose:

```docker compose -f compose.vision.yml up```

note: make sure that u have internet connection on start up (ultralytics needs to download yolov5), then connect to drone

### Dataset generation
*by Thomas Kianek, 01109403*

The dataset to train, test and validate the YOLOv5 network has been created as follows:

1. Capture several videos of the given human silhouette using a mobile phone 
- The videos have been captured at different lighting conditions, at different heights of the mobile phone and whilst walking around the silhouette.
- Additionally the silhouette has been randomly cut into two halves to allow the network to cope with occluded silhouettes.
  
2. Use VLC to split up the caputered videos into single frames
- Every 5 seconds a frame has been extracted.

3. Use the Roboflow framework to generate a dataset out of the images 
- Annotate the silhouette as target on every single image.
- Apply data augmentation: flip, (random) rotation, (random) crop, random shear, blur and exposure adjustments.
- Split up the dataset into training, validation and test set.

The final dataset contains about ~330 images. The size is rather small due to the restrictions of Roboflow's free plan.

## Line Detection/Vision Control (and SLAM)
*by Sebastian Resch, 11820010*

![Line Detection](slam/line_detection.png)

The slam directory has in this state nothing to do with SLAM per se. As usual this project grew historically and the name is a fragment of the past. 
The Dockerfile for this container can be found in the folder "vision_control", the different packages are in the folder "slam"

to get rviz and other display-type programs to show a window, run following command outside the container: 

```
xhost +local:docker
```

To start the docker containers inclusive the slam container, use the normal 

```
docker compose build && docker compose up
```

go into the ppmrob-slam-1 container

```
docker exec -it ppmrob-slam-1 bash
```

in /catkin_ws do 

```
source /opt/ros/noetic/setup.bash
catkin_make
source devel/setup.bash
```

if you got a webcam to simulate the drone start the publish image node

```
rosrun publish_image publish_image.py
```

to start the line detection node:

```
rosrun fast_line_detector fast_line_detector
```

Subscriber: 
* */image* (type: sensor_msgs/Image)

Publisher: 
* */image_converter/output_video* (type: sensor_msgs/Image)
* */line* (type: geometry_msgs/Pose2D)

## Planning and Target Identification
*by Piet Kaul, 00925649*

Behaviour tree based planning node (https://www.behaviortree.dev/).

Drone takes off and flies around until detecting either a border line or a victim. If a line is detected in front of the drone, it turns around (random) and flies in another direction. If a victim is found the drone lands beside it and then takes off again. This behavior is repeated as long as the battery state is above a certain treshold. If the battery state is beneath this threshold, the drone flies back to its initial coordinates.

Checkout [behavior tree](planning_target_identification/BehaviorTree.png) for detailes.

### Working Concept
![Behaviour Tree](behavior_tree/BehaviorTree.png)

In the current state, the line detection container has to be accessed and the necessary line detection command has to executed while running the BT container as well. Also the published image topic has to be remapped.

go into the ppmrob-slam-1 container

```
docker exec -it ppmrob-slam-1 bash
```

to start the line detection node with the remapped topic:

```
source /opt/ros/noetic/setup.bash
catkin_make
source devel/setup.bash
```

```
rosrun fast_line_detector fast_line_detector image:=downward_cam/camera/image
```

This has to happen in order to achieve a working line detection behaviour while using the BT.

On how to create custom nodes, take a look at the BT folders readme file.

Further, for debugging the GUI of the BT, named groot, can be accessed. Here, the ticked nodes and branches can be observed. Also custom trees can be build. For a guide and tutorials on that, take a look at the above mentioned BT web page.

### Ticked Nodes
![Ticked Nodes](behavior_tree/BT_Tick.gif)