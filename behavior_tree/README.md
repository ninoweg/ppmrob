# Behavior Tree
Make sure you have ROS Noetic installed either in a Docker container or otherwise on Ubuntu 20.04 (WSL) by following the steps from https://wiki.ros.org/noetic/Installation/Ubuntu and https://wiki.ros.org/ROS/Tutorials/InstallingandConfiguringROSEnvironment. This installs ROS noetic and sets up a workspace (catkin_ws).

## Install
Tello Python API
```
pip install djitellopy
```
Behavior Tree Package
```
cd catkin_ws/src
git clone https://gitlab.com/ninoweg/ppmrob.git
cd ..
rosdep install --from-paths src --ignore-src -r -y
source /opt/ros/noetic/setup.bash
catkin_make
source devel/setup.bash
```
Groot 
```
sudo apt install qtbase5-dev libqt5svg5-dev libzmq3-dev libdw-dev
cd catkin_ws/src
git clone https://github.com/BehaviorTree/Groot.git
catkin_make
source devel/setup.bash
```
If this failes, check missing dependencies here https://medium.com/teamarimac/groot-with-ros-a8f7855f8e35

## Run
Make sure to have sourced both your local workspace as well as the ROS workspace before running any ROS commands 
```
source /opt/ros/noetic/setup.bash
source ~/catkin_ws/devel/setup.bash
```
Open three terminals. In the first one run:
```
roslaunch drone drone.launch
```
In the second:
```
rosrun behavior_tree behavior_tree_node
```
If the battery level that is published is lower then 50%, wait a few seconds and start again. The battery discharge is simulated in demo mode and reset to 100% when reaching 0%.
 
In the third
```
rosrun groot Groot
```
A GUI should pop-up: double klick on "monitor" and then on "connect". You should be able to observe the behavior tree. 

## Update Tree
Custom nodes and the tree are saved in the corresponding folders. 
Run Groot, double klick on "editor", then on "load tree" and load the custom tree and to edit it or to add new nodes. 

For having your favorite tree to be loaded at the beginning while starting the BT container and groot, the wanted trees path has to be stated in the ```behaviour_tree_node.cpp``` source file.

## Custome Nodes
Custome nodes, consist mainly of two different parts. For one of a C++ file. Here the type of node is defined. The type of BT node (action node etc.), as well as the ROS node type (Subscriber, Publisher etc.).  These .hpp files can be found in the "include" directory.
Then, each node also consists of an .xml file, which ensures the integration into the Behaviour Tree. For Identification, each node has a personal ID and can further contain several features like an input, output, delay etc. The before mentioned .hpps have to be included in the .xml file, depending on the node.
In the directory "interfaces" a data type for input and output can be created. These headers can then be included in the above mentioned .hpp files to ensure propper working callback functions with the incoming sensor data.
Last, each node has to be registered in the "behavior_tree_node.cpp" source file and each header in the "interfaces" directory has to be inserted into the appropriate CMakeLists.txt file.

Now, each node created this way can be integraded in the BT .xml files containing and describing a whole tree. Further these nodes should now also be available under the GUI "Groot".