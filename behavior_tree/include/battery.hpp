#pragma once
#include <ros/ros.h>
#include "bt_subscriber_node.hpp"
#include <std_msgs/Int64.h>
#include <string>

using namespace BT;

class BatteryAction : public RosSubscriberNode<std_msgs::Int64>
{
public:
  BatteryAction(ros::NodeHandle &handle, const std::string &node_name, const NodeConfiguration &conf) : RosSubscriberNode<std_msgs::Int64>(handle, node_name, conf) {}

  static PortsList providedPorts()
  {
    return {InputPort<int>("limit"),};
  }

  NodeStatus onTick(const TopicType& msg) override
  {
    ROS_INFO_STREAM("Battery: " << msg.data << "%");
    int limit = getInput<int>("limit").value();
    if (msg.data >= limit)
      return NodeStatus::SUCCESS;
    return NodeStatus::FAILURE;
  }
};