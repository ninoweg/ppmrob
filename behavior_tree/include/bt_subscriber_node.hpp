// Copyright (c) 2019 Samsung Research America
// Copyright (c) 2020 Davide Faconti
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef BEHAVIOR_TREE_BT_SUBSCRIBER_NODE_HPP_
#define BEHAVIOR_TREE_BT_SUBSCRIBER_NODE_HPP_

#include <behaviortree_cpp_v3/action_node.h>
#include <behaviortree_cpp_v3/bt_factory.h>
#include <ros/ros.h>

using namespace std::chrono;

namespace BT
{

  /**
   * Base Action to implement a ROS Service
   */
  template <class TopicT>
  class RosSubscriberNode : public BT::StatefulActionNode
  {
  public:
    using BaseClass = RosSubscriberNode<TopicT>;
    using TopicType = TopicT;

  protected:
    RosSubscriberNode(ros::NodeHandle &nh, const std::string &name, const BT::NodeConfiguration &conf) : BT::StatefulActionNode(name, conf), node_(nh), msg_received_{false}, was_shutdown_{false} {}

    void callback(const TopicType &msg)
    {
      msg_received_ = true;
      latest_timestamp_ = ros::Time::now();
      latest_message_ = msg;
      setOutput<TopicType>("message", msg);
    }

  public:
    RosSubscriberNode() = delete;

    virtual ~RosSubscriberNode() = default;

    /// These ports will be added automatically if this Node is
    /// registered using RegisterRosAction<DeriveClass>()
    static PortsList providedPorts()
    {
      return {
          InputPort<std::string>("topic_name", "name of the ROS topic"),
          InputPort<unsigned>("delay", 500, "delay to receive first message (milliseconds)"),
          InputPort<unsigned>("timeout", 300, "timeout for considering latest message (milliseconds)"),
          OutputPort<TopicType>("message", "latest message received from subscriber")};
    }

    virtual NodeStatus onTick(const TopicType& msg) = 0;

  protected:
    // The node that will be used for any ROS operations
    ros::NodeHandle &node_;

    ros::Subscriber subscriber_;
    TopicType latest_message_;
    ros::Time latest_timestamp_;
    bool msg_received_;
    bool was_shutdown_;

    NodeStatus onStart() override
    {
      std::string topic_name = getInput<std::string>("topic_name").value();
      subscriber_ = node_.subscribe(topic_name, 10, &RosSubscriberNode::callback, this);
      unsigned delay = getInput<unsigned>("delay").value();
      auto deadline = system_clock::now() + milliseconds(delay);
      while (system_clock::now() <= deadline)
      {
        ros::spinOnce();
        ros::Duration sleep_time(0.01);
        sleep_time.sleep();
        if (msg_received_)
          return NodeStatus::RUNNING;
      }
      return NodeStatus::FAILURE;
    }

    /// method invoked by an action in the RUNNING state.
    NodeStatus onRunning() override
    {
      if (was_shutdown_)
        return onStart();
      auto timeout = getInput<unsigned>("timeout").value() / 1000.0;
      if ((ros::Time::now() - latest_timestamp_).toSec() < timeout) 
      {
        auto result = onTick(latest_message_);
        if (result == NodeStatus::FAILURE)
        {
          subscriber_.shutdown();
          was_shutdown_ = true; 
        }
        return result;
      }
      else
      {
        subscriber_.shutdown(); 
        was_shutdown_ = true;
        return NodeStatus::FAILURE;
      }
    }

    void onHalted() override
    {
      subscriber_.shutdown();
      was_shutdown_ = true;
    }
  };

  /// Method to register the service into a factory.
  /// It gives you the opportunity to set the ros::NodeHandle.
  template <class DerivedT>
  static void RegisterRosSubscriber(BT::BehaviorTreeFactory &factory,
                                    const std::string &registration_ID,
                                    ros::NodeHandle &node_handle)
  {
    NodeBuilder builder = [&node_handle](const std::string &name, const NodeConfiguration &config)
    {
      return std::make_unique<DerivedT>(node_handle, name, config);
    };

    TreeNodeManifest manifest;
    manifest.type = getType<DerivedT>();
    manifest.ports = DerivedT::providedPorts();
    manifest.registration_ID = registration_ID;
    const auto &basic_ports = RosSubscriberNode<typename DerivedT::TopicType>::providedPorts();
    manifest.ports.insert(basic_ports.begin(), basic_ports.end());

    factory.registerBuilder(manifest, builder);
  }

} // namespace BT

#endif // BEHAVIOR_TREE_BT_SUBSCRIBER_NODE_HPP_