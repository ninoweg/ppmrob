#pragma once
#include <ros/ros.h>
#include "bt_subscriber_node.hpp"
#include <interfaces/Detection.h>
#include <string>

using namespace BT;

class DetectionAction : public RosSubscriberNode<interfaces::Detection>
{
public:
  DetectionAction(ros::NodeHandle &handle, const std::string &node_name, const NodeConfiguration &conf) : RosSubscriberNode<interfaces::Detection>(handle, node_name, conf) {}

  static PortsList providedPorts()
  {
    return {InputPort<std::string>("detection_type")};
  }

  NodeStatus onTick(const TopicType& msg) override
  {
    if(msg.pose.empty())
    {
      ROS_INFO_STREAM("Detection: nothing detected.");
      return NodeStatus::FAILURE;
    }
    
    ROS_INFO_STREAM("Detection: type: " << msg.type);
    std::string type = getInput<std::string>("detection_type").value();
    if(msg.type == type)
    {
      // TODO: deal with detection
      return NodeStatus::SUCCESS;
    }
    return NodeStatus::FAILURE;
  }
};