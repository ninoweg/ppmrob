//-------------------------------------------------------------
// This client example is equal to this tutorial:
// http://wiki.ros.org/ROS/Tutorials/WritingServiceClient%28c%2B%2B%29
//-------------------------------------------------------------
#pragma once
#include <ros/ros.h>
#include "bt_action_node.hpp"
#include <interfaces/FlyHomeAction.h>
#include <string>

class FlyHomeServer : public RosActionNode<interfaces::FlyHomeAction>
{

public:
  FlyHomeServer(ros::NodeHandle &handle, const std::string &name, const NodeConfiguration &conf) : RosActionNode<interfaces::FlyHomeAction>(handle, name, conf) {}

  static PortsList providedPorts()
  {
    return {
        OutputPort<int>("result")};
  }

  bool sendGoal(GoalType &goal) override
  {
    ROS_INFO("FlyHomeAction: sending request");
    return true;
  }

  NodeStatus onResult(const ResultType &res) override
  {
    ROS_INFO("FlyHomeAction: result received");
    if (res.success)
    {
      setOutput<int>("result", res.success);
      return NodeStatus::SUCCESS;
    }
    else
    {
      ROS_ERROR("FlyHomeAction replied something unexpected: %d", res.success);
      return NodeStatus::FAILURE;
    }
  }

  virtual NodeStatus onFailedRequest(FailureCause failure) override
  {
    ROS_ERROR("FlyHomeAction request failed %d", static_cast<int>(failure));
    return NodeStatus::FAILURE;
  }

  void halt() override
  {
    if (status() == NodeStatus::RUNNING)
    {
      ROS_WARN("FlyHomeAction halted");
      BaseClass::halt();
    }
  }

private:
  int expected_result_;
};