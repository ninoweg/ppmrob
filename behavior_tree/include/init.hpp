//-------------------------------------------------------------
// This client example is equal to this tutorial:
// http://wiki.ros.org/ROS/Tutorials/WritingServiceClient%28c%2B%2B%29
//-------------------------------------------------------------
#pragma once
#include <ros/ros.h>
#include "bt_service_node.hpp"
#include <interfaces/Init.h>
#include <interfaces/InitRequest.h>
#include <interfaces/InitResponse.h>
#include <string>

using namespace BT;

class InitAction : public RosServiceNode<interfaces::Init>
{
public:
  InitAction(ros::NodeHandle &handle, const std::string &node_name, const NodeConfiguration &conf) : RosServiceNode<interfaces::Init>(handle, node_name, conf) {}

  static PortsList providedPorts()
  {
    return {
        OutputPort<bool>("success")};
  }
  
  void sendRequest(RequestType &request) override
  {
    ROS_INFO("Init: sending request");
  }

  NodeStatus onResponse(const ResponseType &rep) override
  {
    ROS_INFO("Init: response received");
    if (rep.success)
    {
      return NodeStatus::SUCCESS;
    }
    else
    {
      return NodeStatus::FAILURE;
    }
  }

  virtual NodeStatus onFailedRequest(RosServiceNode::FailureCause failure) override
  {
    ROS_ERROR("Init request failed %d", static_cast<int>(failure));
    return NodeStatus::FAILURE;
  }
};