#pragma once
#include <ros/ros.h>
#include "bt_subscriber_node.hpp"
#include <interfaces/Line_Detection.h>
#include <string>
#include <geometry_msgs/Pose2D.h>

using namespace BT;

class Line_DetectionAction : public RosSubscriberNode<interfaces::Line_Detection>
{
public:
  Line_DetectionAction(ros::NodeHandle &handle, const std::string &node_name, const NodeConfiguration &conf) : RosSubscriberNode<interfaces::Line_Detection>(handle, node_name, conf) {}

 static PortsList providedPorts()
  {
    return {InputPort<std::string>("detection_type")};
  }

  NodeStatus onTick(const TopicType& msg) override
  {
    
    
    //ROS_INFO_STREAM("Line_Detection: type: " << msg.type);
    std::string type = getInput<std::string>("detection_type").value();
    if(msg.x && msg.y && msg.theta )
    {
      // TODO: deal with detection
      return NodeStatus::SUCCESS;
    }
    return NodeStatus::FAILURE;
  }
};