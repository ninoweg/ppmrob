//-------------------------------------------------------------
// This client example is equal to this tutorial:
// http://wiki.ros.org/ROS/Tutorials/WritingServiceClient%28c%2B%2B%29
//-------------------------------------------------------------
#pragma once
#include <ros/ros.h>
#include "bt_service_node.hpp"
#include <interfaces/Move.h>
#include <interfaces/MoveRequest.h>
#include <interfaces/MoveResponse.h>
#include <string>

using namespace BT;

class MoveAction : public RosServiceNode<interfaces::Move>
{
public:
  MoveAction(ros::NodeHandle &handle, const std::string &node_name, const NodeConfiguration &conf) : RosServiceNode<interfaces::Move>(handle, node_name, conf) {}

  static PortsList providedPorts()
  {
    return {
        InputPort<std::string>("x"),
        InputPort<std::string>("y"),
        InputPort<std::string>("z"),
        InputPort<std::string>("speed"),
        InputPort<std::string>("rotate"),
        OutputPort<bool>("success")};
  }

  void sendRequest(RequestType &request) override
  {
    getInput("x", request.x);
    getInput("y", request.y);
    getInput("z", request.z);
    getInput("speed", request.speed);
    getInput("rotate", request.rotate);
    ROS_INFO("Move: sending request");
  }

  NodeStatus onResponse(const ResponseType &rep) override
  {
    ROS_INFO("Move: response received");
    if (rep.success)
    {
      setOutput<bool>("success", rep.success);
      return NodeStatus::SUCCESS;
    }
    else
    {
      return NodeStatus::FAILURE;
    }
  }

  virtual NodeStatus onFailedRequest(RosServiceNode::FailureCause failure) override
  {
    ROS_ERROR("Move request failed %d", static_cast<int>(failure));
    return NodeStatus::FAILURE;
  }
};