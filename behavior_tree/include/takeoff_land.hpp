//-------------------------------------------------------------
// This client example is equal to this tutorial:
// http://wiki.ros.org/ROS/Tutorials/WritingServiceClient%28c%2B%2B%29
//-------------------------------------------------------------
#pragma once
#include <ros/ros.h>
#include "bt_service_node.hpp"
#include <interfaces/TakeOffLand.h>
#include <interfaces/TakeOffLandRequest.h>
#include <interfaces/TakeOffLandResponse.h>
#include <string>

using namespace BT;

class TakeoffLandAction : public RosServiceNode<interfaces::TakeOffLand>
{
public:
  TakeoffLandAction(ros::NodeHandle &handle, const std::string &node_name, const NodeConfiguration &conf) : RosServiceNode<interfaces::TakeOffLand>(handle, node_name, conf) {}

  static PortsList providedPorts()
  {
    return {
        InputPort<std::string>("command"),
        OutputPort<bool>("success")};
  }

  void sendRequest(RequestType &request) override
  {
    getInput("command", request.command);
    ROS_INFO("TakeOffLand: sending request");
  }

  NodeStatus onResponse(const ResponseType &rep) override
  {
    ROS_INFO("TakeOffLand: response received");
    if (rep.success)
    {
      setOutput<bool>("success", rep.success);
      return NodeStatus::SUCCESS;
    }
    else
    {
      return NodeStatus::FAILURE;
    }
  }

  virtual NodeStatus onFailedRequest(RosServiceNode::FailureCause failure) override
  {
    ROS_ERROR("TakeOffLand request failed %d", static_cast<int>(failure));
    return NodeStatus::FAILURE;
  }
};