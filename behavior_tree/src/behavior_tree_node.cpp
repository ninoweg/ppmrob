#include <ros/ros.h>
#include <behaviortree_cpp_v3/bt_factory.h>
#include <behaviortree_cpp_v3/loggers/bt_zmq_publisher.h>

#include "takeoff_land.hpp"
#include "battery.hpp"
#include "move.hpp"
#include "flyhome.hpp"
#include "detection.hpp"
#include "init.hpp"
#include "line_detection.hpp"

using namespace BT;

int main(int argc, char **argv)
{
  ros::init(argc, argv, "behavior_tree_node");
  ros::NodeHandle nh;

  BehaviorTreeFactory factory;

  // RegisterRosSubscriber
  
  RegisterRosSubscriber<DetectionAction>(factory, "Detection", nh);
  RegisterRosSubscriber<BatteryAction>(factory, "Battery", nh);
  RegisterRosSubscriber<Line_DetectionAction>(factory, "Line_Detection", nh);

  RegisterRosService<TakeoffLandAction>(factory, "TakeoffLand", nh);
  RegisterRosService<InitAction>(factory, "Init", nh);
  RegisterRosService<MoveAction>(factory, "Move", nh);

  RegisterRosAction<FlyHomeServer>(factory, "FlyHome", nh);

  auto tree = factory.createTreeFromFile("/catkin_ws/src/behavior_tree/tree/tree_advanced.xml");
  
  PublisherZMQ publisher_zmq(tree);

  NodeStatus status = NodeStatus::IDLE;

  while( ros::ok() && (status == NodeStatus::IDLE || status == NodeStatus::RUNNING))
  {
    ros::spinOnce();
    status = tree.tickRoot();
    std::cout << status << std::endl;
    ros::Duration sleep_time(0.1);
    sleep_time.sleep();
  }

  return 0;
}