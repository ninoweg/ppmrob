import rospy
from std_msgs.msg import Header
from vision_msgs.msg import Detection2DArray, Detection2D, BoundingBox2D
from geometry_msgs.msg import Pose2D
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
from interfaces.msg import Detection
import torch
import cv2
import threading
import time

class ComputerVision:
    def __init__(self):
        self.pub_detections = rospy.Publisher("/detections", Detection, queue_size=10)
        self.number_subscriber = rospy.Subscriber("/image", Image, self.callback_image)
        self.msg_count = 0
        self.model = torch.hub.load('ultralytics/yolov5', 'custom',
                                    path='/catkin_ws/src/ppmrob/computer_vision/scripts/best.pt', force_reload=True)
        self.model.eval()
        self.bridge = CvBridge()

        self.lock = threading.Lock()

        if torch.cuda.is_available():
            self.device = torch.device('cuda')
            self.model.to(self.device)
        else:
            self.device = torch.device('cpu')

        rospy.loginfo("Started computer vision node")

    # def callback_image(self, msg):
    #     threading.Thread(target=self.process_image, args=(msg,)).start()

    def callback_image(self, msg):
        # with self.lock:
        start_time = time.time()  # Capture start time
        rospy.loginfo(f"Received image with encoding: {msg.encoding}")
        try:
            cv_img = self.bridge.imgmsg_to_cv2(msg, desired_encoding="rgb8")
            # rosbag says image is rgb8, but if mono in real, just comment in this line
            # grey_img = self.bridge.imgmsg_to_cv2(msg, "mono8")
            grey_img = cv2.cvtColor(cv_img, cv2.COLOR_RGB2GRAY)

        except CvBridgeError as e:
            rospy.logerr(f"Failed to convert image: {e}")
            return

        results = self.model(grey_img)
        yolo_detections = results.xyxy[0].numpy()

        for det in yolo_detections:
            detection = Detection()
            pose = Pose2D()
            pose.x = (det[0] + det[2]) / 2  # x center
            pose.y = (det[1] + det[3]) / 2  # y center
            pose.theta = 0

            detection.pose.append(pose)
            rospy.loginfo(f"Detected class {int(det[5])} with confidence {det[4]} at position ({pose.x}, {pose.y})")

            # bbox = BoundingBox2D()
            # bbox.center.x = (det[0] + det[2]) / 2  # x center
            # bbox.center.y = (det[1] + det[3]) / 2  # y center
            # bbox.size_x = det[2] - det[0]  # width
            # bbox.size_y = det[3] - det[1]  # height
            # detection.bbox = bbox
            # detection.source_img = msg
            # detections_msg.detections.append(pose)

            self.pub_detections.publish(detection)
            end_time = time.time()  # Capture end time
            duration = end_time - start_time  # Calculate duration
            rospy.loginfo(f"Image processing duration: {duration:.4f} seconds")


if __name__ == '__main__':
    rospy.init_node('computer_vision')
    drone = ComputerVision()
    rospy.spin()