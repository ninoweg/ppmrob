import torch
import pandas as pd
# from vision_msgs.msg import Detection2D, Detection2DArray, BoundingBox2D
# from std_msgs.msg import Header
model = torch.hub.load('ultralytics/yolov5', 'custom', path='best.pt')
# Image
img = 'file.jpg'
# Inference
results = model(img)
# Results, change the flowing to: results.show()
results.show()  # or .show(), .save(), .crop(), .pandas(), etc

df = results.pandas().xyxy[0]  # Get the DataFrame for the first (and typically only) image processed

# Print the bounding box coordinates of each detected object
for index, row in df.iterrows():
    bbox = row[['xmin', 'ymin', 'xmax', 'ymax']].values
    print(f"Object {index + 1}: Bounding Box: {bbox}")


print("How many detected = ", len(df))