#!/usr/bin/env python
import rospy
from std_msgs.msg import Int64, Header
from geometry_msgs.msg import Pose2D, Twist
from sensor_msgs.msg import Imu, Image
from nav_msgs.msg import Odometry
from interfaces.srv import TakeOffLand, Move, Init
from cv_bridge import CvBridge
from tf.transformations import quaternion_from_euler
from geometry_msgs.msg import TransformStamped
import tf2_ros
from djitellopy import Tello
import numpy as np

class Drone:
    def __init__(self):
        self.tf_broadcaster = tf2_ros.TransformBroadcaster()
        # self.pub_odom = rospy.Publisher("/odom", Odometry, queue_size=10)
        self.pub_imu = rospy.Publisher("/imu", Imu, queue_size=10)
        self.pub_imu_odom_timer = rospy.Timer(rospy.Duration(0.1), callback=self.callback_pub_imu_odom_timer)
        self.imu_msg_count = 0
        self.odom_msg_count = 0

        self.pub_img = rospy.Publisher("/image", Image, queue_size=10)
        self.pub_img_timer = rospy.Timer(rospy.Duration(0.2), callback=self.callback_pub_img_timer)
        self.bridge = CvBridge()
        
        self.pub_battery = rospy.Publisher("/battery", Int64, queue_size=10)
        self.pub_battery_timer = rospy.Timer(rospy.Duration(0.1), callback=self.callback_pub_battery_timer)
        self.battery = 0

        self.srv_init = rospy.Service("/init", Init, self.callback_init)
        self.srv_takeoff = rospy.Service("/takeoff_land", TakeOffLand, self.callback_takeoff_land)
        self.srv_move = rospy.Service("/move", Move, self.callback_move)

        self.sub_control = rospy.Subscriber("/control", Twist, self.callback_control)

        self.tello = None
        self.connected = False
        self.frame_read = None
        
        self.TO_RAD = np.pi / 180.0
        
        self.g = 9.80665 # m/s2

        self.last_time = rospy.Time.now()

        self.x = 0.0
        self.y = 0.0
        self.z = 0.8
        self.yaw = 0.0

        rospy.loginfo("Started drone node")

    def callback_pub_battery_timer(self, timer):
        if not self.connected:
            return
        self.battery = self.tello.get_battery()
        battery_msg = Int64(int(self.battery))
        self.pub_battery.publish(battery_msg)
        self.tello.send_keepalive()

    def callback_pub_img_timer(self, timer):
        if not self.connected:
            return
        if not self.frame_read:
            rospy.loginfo("Frame read empty")
            return
        frame = None 
        try:
            frame = self.frame_read.frame.copy()
            frame = frame[0:240, :, :]
            img_msg = self.bridge.cv2_to_imgmsg(frame, encoding="rgb8")
            img_msg.header.frame_id = "base_link"
            img_msg.header.stamp = rospy.Time.now()
            self.pub_img.publish(img_msg)
            # cv2.imwrite("img.png", frame)
        except:
            rospy.loginfo("Could not convert frame")
        

    def callback_pub_imu_odom_timer(self, timer): 
        if not self.connected:
            return

        # angles in degree
        roll = self.tello.get_roll() * self.TO_RAD
        pitch = self.tello.get_pitch() * self.TO_RAD
        yaw = self.tello.get_yaw() * self.TO_RAD

        # acceleration in 0.001g = 0.001 * 9.80665 m/s2
        ax = -self.tello.get_acceleration_x() * self.g / 1000.0 # x seems to be inverted?!
        ay = self.tello.get_acceleration_y() * self.g / 1000.0
        az = self.tello.get_acceleration_z() * self.g / 1000.0

        q = quaternion_from_euler(roll, pitch, yaw)

        imu_msg = Imu()
        imu_msg.header = Header(self.imu_msg_count, rospy.Time.now(), "base_link")
        imu_msg.orientation.x = q[0]
        imu_msg.orientation.y = q[1]
        imu_msg.orientation.z = q[2]
        imu_msg.orientation.w = q[3]
        imu_msg.orientation_covariance = [-1.0] * 9
        imu_msg.linear_acceleration.x = ax
        imu_msg.linear_acceleration.y = ay
        imu_msg.linear_acceleration.z = az
        imu_msg.linear_acceleration_covariance = [-1.0] * 9
        self.imu_msg_count += 1


    def callback_init(self, req):
        rospy.loginfo("Try to connect to drone...")
        self.tello = Tello(host="192.168.10.1", vs_udp="11111")
        try:
            self.tello.connect(wait_for_state=True)
        except:
            rospy.loginfo("Could not connect to drone.")
            return False
        self.tello.set_video_direction(Tello.CAMERA_DOWNWARD)
        # self.tello.set_video_fps(Tello.FPS_5)
        self.tello.streamon()
        self.frame_read = self.tello.get_frame_read()
        rospy.loginfo("Successfully initialized drone.")
        self.connected = True
        return True

    def callback_takeoff_land(self, req):
        if not self.connected:
            rospy.loginfo("Tello not connected")
            return False 
        if req.command == "takeoff":
            rospy.loginfo("takeoff")
            self.update_odom(0.0, 0.0, 0.0, 0.0)
            self.tello.takeoff()
            return True
        elif req.command == "land":
            rospy.loginfo("land")
            self.update_odom(0.0, 0.0, 0.0, 0.0)
            self.tello.land()
            return True  
        rospy.loginfo("Not a valid request. Choose between 'land' and 'takeoff'.")          
        return False
    
    def callback_move(self, req):
        if not self.connected:
            return False
        # first check if "rotate_clockwise" is requested
        if req.rotate >= 1 and req.rotate <= 360:
            rospy.loginfo("Send rotate_clockwise request to drone.")
            self.tello.rotate_clockwise(req.rotate)
            self.update_odom(0.0, 0.0, 0.0, -req.rotate * self.TO_RAD)
            return True
        # if not, check if arguments for "go_xyz_speed" are valid
        if req.x < -500 or req.x > 500:
            rospy.loginfo("x not in valid range [-500, 500]")
            return False
        if req.y < -500 or req.y > 500:
            rospy.loginfo("y not in valid range [-500, 500]")
            return False
        if req.z < -500 or req.z > 500:
            rospy.loginfo("z not in valid range [-500, 500]")
            return False
        if req.speed < 10 or req.speed > 100:
            rospy.loginfo("speed not in valid range [10, 100]")
            return False
        rospy.loginfo("Send go_xyz_speed request to drone: x: " + str(req.x) + ", y: " + str(req.y) + ", z: " + str(req.z) + ", speed: " + str(req.speed))
        self.tello.go_xyz_speed(int(req.x), int(req.y), int(req.z), int(req.speed)) 
        self.update_odom(req.x, req.y, req.z, 0.0)
        return True
    
    def callback_control(self, msg):
        if msg.linear.x < -100 and msg.linear.x > 100:
            rospy.loginfo("x not in valid range [-100, 100]")
            return False
        if msg.linear.y < -100 and msg.linear.y > 100:
            rospy.loginfo("y not in valid range [-100, 100]")
            return False
        if msg.linear.z < -100 and msg.linear.z > 100:
            rospy.loginfo("z not in valid range [-100, 100]")
            return False
        if msg.angular.z < 10 and msg.angular.z > 100:
            rospy.loginfo("yaw not in valid range [100, 100]")
            return False
        self.tello.send_rc_control(int(msg.linear.y), int(msg.linear.x), int(msg.linear.z), int(-msg.angular.z))

    def update_odom(self, delta_x, delta_y, delta_z, delta_yaw):

        self.yaw += delta_yaw
        if self.yaw > 2*np.pi:
            self.yaw -= 2*np.pi
        elif self.yaw < 0:
            self.yaw += 2*np.pi

        self.x += (delta_x * np.cos(self.yaw) - delta_y * np.sin(self.yaw))
        self.y += (delta_x * np.sin(self.yaw) + delta_y * np.cos(self.yaw))
        self.z += delta_z

        q = quaternion_from_euler(0.0, 0.0, self.yaw)

        # odom_msg = Odometry()
        # odom_msg.header = Header(self.odom_msg_count, rospy.Time.now(), "odom")
        # odom_msg.child_frame_id = "base_link"
        # odom_msg.pose.pose.orientation.x = q[0]
        # odom_msg.pose.pose.orientation.y = q[1]
        # odom_msg.pose.pose.orientation.z = q[2]
        # odom_msg.pose.pose.orientation.w = q[3]
        # odom_msg.pose.pose.position.x = self.x
        # odom_msg.pose.pose.position.y = self.y
        # odom_msg.pose.pose.position.z = self.z

        # self.pub_odom.publish(odom_msg)

        t = TransformStamped()
        t.header.stamp = rospy.Time.now()
        t.header.frame_id = "odom"
        t.child_frame_id = "base_link"
        t.transform.translation.x = self.x / 100.0
        t.transform.translation.y = self.y / 100.0
        t.transform.translation.z = self.z / 100.0
        t.transform.rotation.x = q[0]
        t.transform.rotation.y = q[1]
        t.transform.rotation.z = q[2]
        t.transform.rotation.w = q[3]

        self.tf_broadcaster.sendTransform(t)

        # self.odom_msg_count += 1

if __name__ == '__main__':
    rospy.init_node('drone')
    drone = Drone()
    rospy.spin()