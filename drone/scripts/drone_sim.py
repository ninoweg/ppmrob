#!/usr/bin/env python
import rospy
from std_msgs.msg import Int64, Header
from geometry_msgs.msg import PoseStamped 
from sensor_msgs.msg import Imu, Image
from nav_msgs.msg import Odometry
from interfaces.srv import TakeOffLand, Move, Init
from hector_uav_msgs.srv import EnableMotors
from hector_uav_msgs.msg import TakeoffAction, TakeoffGoal, LandingAction, LandingGoal
from cv_bridge import CvBridge
from tf.transformations import quaternion_from_euler, euler_from_quaternion
from geometry_msgs.msg import TransformStamped
import numpy as np
import actionlib

class DroneSim:
    def __init__(self):
        self.pub_odom = rospy.Publisher("/odom", Odometry, queue_size=10)
        self.pub_cmd = rospy.Publisher("/command/pose", PoseStamped, queue_size=10)
        
        self.pub_battery = rospy.Publisher("/battery", Int64, queue_size=10)
        self.pub_battery_timer = rospy.Timer(rospy.Duration(0.1), callback=self.callback_pub_battery_timer)
        self.battery = 100

        self.sub_pose = rospy.Subscriber("/ground_truth_to_tf/pose", PoseStamped, self.callback_pose)

        self.srv_init = rospy.Service("/init", Init, self.callback_init)
        self.srv_takeoff = rospy.Service("/takeoff_land", TakeOffLand, self.callback_takeoff_land)
        self.srv_move = rospy.Service("/move", Move, self.callback_move)

        self.tello = None
        self.connected = False
        self.frame_read = None

        self.pose = None
        
        self.TO_RAD = np.pi / 180.0
        
        self.g = 9.80665 # m/s2

        self.last_time = rospy.Time.now()

        self.x = 0.0
        self.y = 0.0
        self.z = 0.0
        self.vx = 0.0
        self.vy = 0.0
        self.vz = 0.0

        rospy.loginfo("Started drone node")

    def callback_pose(self, msg):
        self.pose = msg

    def callback_pub_battery_timer(self, timer):
        if not self.connected:
            return
        self.battery -= 0.025
        battery_msg = Int64(int(self.battery))
        self.pub_battery.publish(battery_msg)

    def callback_init(self, req):
        rospy.loginfo("Try to connect to drone...")
        rospy.wait_for_service("enable_motors")
        enable = True
        try:
            enable_motors = rospy.ServiceProxy('enable_motors', EnableMotors)
            resp = enable_motors(enable)
            self.connected = resp.success
            if self.connected:
                rospy.loginfo("Successfully initialized drone.")
            else:
                rospy.loginfo("Could not connect to drone.")
            return resp.success
        except rospy.ServiceException as e:
            print("Service call failed: %s"%e)
    

    def callback_takeoff_land(self, req):
        if not self.connected:
            rospy.loginfo("Tello not connected")
            return False 
        if req.command == "takeoff":
            rospy.loginfo("takeoff")
            client = actionlib.SimpleActionClient('/action/takeoff', TakeoffAction)
            client.wait_for_server()
            goal = TakeoffGoal()
            client.send_goal(goal)
            client.wait_for_result()
            # return client.get_result()
            return True
        elif req.command == "land":
            rospy.loginfo("land")
            client = actionlib.SimpleActionClient('/action/landing', LandingAction)
            client.wait_for_server()
            landing_zone = PoseStamped()
            goal = LandingGoal(landing_zone=landing_zone)
            client.send_goal(goal)
            client.wait_for_result()
            return True  
        rospy.loginfo("Not a valid request. Choose between 'land' and 'takeoff'.")          
        return False
    
    def callback_move(self, req):
        if not self.connected:
            return False
        # first check if "rotate_clockwise" is requested
        if req.rotate < 0 or req.rotate >= 360:
            rospy.loginfo("rotate needs to be in valid range [0, 360]")
            return False
        # if not, check if arguments for "go_xyz_speed" are valid
        if req.x < -500 or req.x > 500:
            rospy.loginfo("x not in valid range [-500, 500]")
            return False
        if req.y < -500 or req.y > 500:
            rospy.loginfo("y not in valid range [-500, 500]")
            return False
        if req.z < -500 or req.z > 500:
            rospy.loginfo("z not in valid range [-500, 500]")
            return False
        if req.speed < 10 or req.speed > 100:
            rospy.loginfo("speed not in valid range [10, 100]")
            return False
        
        rospy.loginfo("Send go_xyz_speed/rotate request to drone")
        (_, _, current_yaw) = euler_from_quaternion([self.pose.pose.orientation.x, 
                                                         self.pose.pose.orientation.y, 
                                                         self.pose.pose.orientation.z, 
                                                         self.pose.pose.orientation.w])
        goal_pose = PoseStamped()
        q = quaternion_from_euler(0.0, 0.0, current_yaw + req.rotate * self.TO_RAD)
        goal_pose.header = self.pose.header
        goal_pose.pose.position.x = self.pose.pose.position.x + (req.x * 0.01) * np.cos(current_yaw)
        goal_pose.pose.position.y = self.pose.pose.position.y + (req.y * 0.01) * np.sin(current_yaw)
        goal_pose.pose.position.z = self.pose.pose.position.z + (req.z * 0.01)
        goal_pose.pose.orientation.x = q[0]
        goal_pose.pose.orientation.y = q[1]
        goal_pose.pose.orientation.z = q[2]
        goal_pose.pose.orientation.w = q[3]
        (_, _, goal_yaw) = euler_from_quaternion([goal_pose.pose.orientation.x, 
                                                    goal_pose.pose.orientation.y, 
                                                    goal_pose.pose.orientation.z, 
                                                    goal_pose.pose.orientation.w])
        reached_goal, timeout = False, False
        start_time = rospy.Time.now()
        while not reached_goal and not timeout:
            (_, _, current_yaw) = euler_from_quaternion([self.pose.pose.orientation.x, 
                                                            self.pose.pose.orientation.y, 
                                                            self.pose.pose.orientation.z, 
                                                            self.pose.pose.orientation.w])
            delta_pos = np.sqrt((self.pose.pose.orientation.x - goal_pose.pose.orientation.x) ** 2 + 
                                (self.pose.pose.orientation.y - goal_pose.pose.orientation.y) ** 2 + 
                                (self.pose.pose.orientation.z - goal_pose.pose.orientation.z) ** 2)
            delta_yaw = abs(current_yaw - goal_yaw)
            rospy.loginfo("Delta position: " + str(delta_pos))
            rospy.loginfo("Delta yaw: " + str(delta_yaw))
            if delta_yaw < 0.1 and delta_pos < 0.1:
                rospy.loginfo("Reached goal")
                reached_goal = True
            if (rospy.Time.now() - start_time).to_sec() > 10.0:
                rospy.loginfo("Timeout")
                timeout = True
            self.pub_cmd.publish(goal_pose)
            rospy.sleep(duration=0.1)
        if reached_goal and not timeout:
            return True
        return False
    

if __name__ == '__main__':
    rospy.init_node('drone')
    drone = DroneSim()
    rospy.spin()