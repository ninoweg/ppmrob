#!/usr/bin/env python
import rospy
from std_msgs.msg import Header
from sensor_msgs.msg import Imu
from nav_msgs.msg import Odometry
from tf.transformations import euler_from_quaternion
from geometry_msgs.msg import TransformStamped
import tf2_ros
import numpy as np

class Odometer:
    def __init__(self):
        self.tf_broadcaster = tf2_ros.TransformBroadcaster()
        
        self.pub_odom = rospy.Publisher("/odom", Odometry, queue_size=10)
        self.odom_frame_id = "world"
        self.odom_msg_count = 0
        
        self.sub_imu = rospy.Subscriber("/raw_imu", Imu, self.callback_imu)
        self.pub_imu = rospy.Publisher("/imu", Imu, queue_size=10)
        self.last_msg = None
        
        self.x = 0.0
        self.y = 0.0
        self.z = 0.0
        self.vx = 0.0
        self.vy = 0.0
        self.vz = 0.0

        self.g = 9.80665 # m/s2
    
        rospy.loginfo("Started odometry node")


    def callback_imu(self, msg: Imu): 
        
        msg_cp = msg

        msg_cp.header.frame_id = "base_link"

        self.pub_imu.publish(msg_cp)

        # if not self.last_msg:
        #     self.last_msg = msg
        #     return
        
        # (roll, pitch, yaw) = euler_from_quaternion([msg.orientation.x, msg.orientation.y, msg.orientation.z, msg.orientation.w])

        # dt = (msg.header.stamp - self.last_msg.header.stamp).to_sec()

        # self.vx += msg.linear_acceleration.x * dt
        # self.vy += msg.linear_acceleration.y * dt
        # self.vz += (msg.linear_acceleration.z  - self.g) * dt

        # delta_x = self.vx * dt
        # delta_y = self.vy * dt
        # delta_z = self.vz * dt
        # self.x += (delta_x * np.cos(yaw) - delta_y * np.sin(yaw))
        # self.y += (delta_x * np.sin(yaw) + delta_y * np.cos(yaw))
        # self.z += delta_z

        # odom_msg = Odometry()
        # odom_msg.header = Header(self.odom_msg_count, msg.header.stamp, self.odom_frame_id)
        # odom_msg.child_frame_id = msg.header.frame_id
        # odom_msg.pose.pose.orientation = msg.orientation
        # odom_msg.pose.pose.position.x = self.x
        # odom_msg.pose.pose.position.y = self.y
        # odom_msg.pose.pose.position.z = self.z
        # odom_msg.twist.twist.linear.x = self.vx
        # odom_msg.twist.twist.linear.y = self.vy
        # odom_msg.twist.twist.linear.z = self.vz

        # t = TransformStamped()
        # t.header = Header(self.odom_msg_count, msg.header.stamp, self.odom_frame_id)
        # t.header.frame_id = self.odom_frame_id
        # t.child_frame_id = msg.header.frame_id
        # t.transform.translation.x = self.x
        # t.transform.translation.y = self.y
        # t.transform.translation.z = self.z
        # t.transform.rotation = msg.orientation

        # self.tf_broadcaster.sendTransform(t)

        # self.pub_odom.publish(odom_msg)
        # self.odom_msg_count += 1
        # self.last_msg = msg


if __name__ == '__main__':
    rospy.init_node('odometry')
    odom = Odometer()
    rospy.spin()