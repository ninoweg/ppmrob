#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/ximgproc.hpp>
#include <opencv2/imgcodecs.hpp>
#include <geometry_msgs/Pose2D.h>
#include <cmath>
#include <vector>
#include <map>
#include <fstream>

//#include <dynamic_reconfigure/server.h>
//#include <fast_line_detector/fast_line_detectorConfig.h>


using namespace std;
using namespace cv;
using namespace cv::ximgproc;

//static const std::string OPENCV_WINDOW = "Image window";
static double PARALLEL_TOLERANCE = 0.1;
static double DISTANCE_TOLERANCE = 20.0;

static int length_threshold = 40;
static float distance_threshold = 1.41421356f;
static double canny_th1 = 50.0;
static double canny_th2 = 50.0;
static int canny_aperture_size = 3;
static bool do_merge = false;

static int gaussian_kernel_size = 3;
static int picture_resize_factor = 3;


double calculateSlope(const Vec4f& line);
bool areParallel(double slope1, double slope2);
double calculateLength(const Vec4f& line);
Point2f calculateMidpoint(const Vec4f& line);
double pointLineDistance(const Point2f& point, const Vec4f& line);
double lineSegmentDistance(const Vec4f& line1, const Vec4f& line2);

class ImageConverter
{
  ros::NodeHandle nh_;
  image_transport::ImageTransport it_;
  image_transport::Subscriber image_sub_;
  image_transport::Publisher image_pub_;
  ros::Publisher line_pub;

public:
  ImageConverter() : it_(nh_)
  {
    // Subscrive to input video feed and publish output video feed
    image_sub_ = it_.subscribe("/image", 1,
      &ImageConverter::imageCb, this);
    image_pub_ = it_.advertise("/image_converter/output_video", 1);
    line_pub = nh_.advertise<geometry_msgs::Pose2D>("line", 1000);
    
    //cv::namedWindow(OPENCV_WINDOW);
  }

  ~ImageConverter()
  {
    //cv::destroyWindow(OPENCV_WINDOW);
  }

  void imageCb(const sensor_msgs::ImageConstPtr& msg)
  {
    Ptr<FastLineDetector> fld = createFastLineDetector(length_threshold,
            distance_threshold, canny_th1, canny_th2, canny_aperture_size,
            do_merge);
    vector<Vec4f> lines_fld;    

    
    cv_bridge::CvImagePtr cv_ptr;
    cv_bridge::CvImagePtr res_cv_ptr;
    try
    {
      cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
    }
    catch (cv_bridge::Exception& e)
    {
      ROS_ERROR("cv_bridge exception: %s", e.what());
      return;
    }

    Mat image = cv_ptr->image;
    Mat greyimage;
    Mat resized_image;
    Mat blurred_image;
    
    cvtColor(image, greyimage, CV_BGR2GRAY);

    int resize_factor = picture_resize_factor;
    resize(greyimage, resized_image, cv::Size(greyimage.cols / resize_factor, greyimage.rows / resize_factor), 0, 0, cv::INTER_AREA);

    GaussianBlur(resized_image, blurred_image, Size(gaussian_kernel_size, gaussian_kernel_size), 0);

    fld->detect(blurred_image, lines_fld);

    // // Define the output file path
    // const std::string OUTPUT_FILE_PATH = "/catkin_ws/src/slam/lines_histogram.txt"; // Change this to your desired file path

    // // Open the output file
    // std::ofstream outputFile(OUTPUT_FILE_PATH, std::ios_base::app);

    // if (!outputFile.is_open()) {
    //     ROS_ERROR("Unable to open file for writing: %s", OUTPUT_FILE_PATH.c_str());
    //     return;
    // }

    double dist_tol = DISTANCE_TOLERANCE/resize_factor;

    // Create a vector to store the lines that meet the criteria
    vector<Vec4f> parallel_lines;

    // Iterate through each line
    for (size_t i = 0; i < lines_fld.size(); ++i) {
        const Vec4f& line1 = lines_fld[i];
        double slope1 = calculateSlope(line1);
        
        for (size_t j = i + 1; j < lines_fld.size(); ++j) {
            const Vec4f& line2 = lines_fld[j];
            double slope2 = calculateSlope(line2);

            // Check if the lines are parallel
            if (areParallel(slope1, slope2)) {
                // Check if the midpoints are within the distance tolerance
                double distance = lineSegmentDistance(line1, line2);
                if (distance > 4.3 && distance < 5.16) {// für resizefactor 3
                //if (distance > 6.3 && distance < 8.3) { // für resizefactor 2
                    // Add both lines to the vector if they meet the criteria
                    parallel_lines.push_back(line1);
                    parallel_lines.push_back(line2);
                    
                    // double length = calculateLength(line1);
                    // outputFile << length << "   " << slope1 << "    " << distance << "\n";
                    // length = calculateLength(line2);
                    // outputFile << length << "   " << slope2 << "    " << distance << "\n";
                }
            }
        }
    }

    Vec4f longest_line;
    double max_length = 0.0;
    for (const Vec4f& line : parallel_lines) {
        double length = calculateLength(line);
        if (length > max_length) {
            max_length = length;
            longest_line = line;
        }
    }



    // Draw the filtered parallel lines
    fld->drawSegments(blurred_image, parallel_lines);

    //fld->drawSegments(blurred_image, lines_fld);

    resize(blurred_image, blurred_image, cv::Size(blurred_image.cols * resize_factor, blurred_image.rows * resize_factor), 0, 0, cv::INTER_AREA);


    cv_ptr->image = blurred_image;
    // Update GUI Window
    //cv::resizeWindow(OPENCV_WINDOW, 500, 500);
    //cv::imshow(OPENCV_WINDOW, cv_ptr->image);
    //cv::waitKey(3);

    //outputFile.close(); // Close the file
    
    // Output modified video stream
    geometry_msgs::Pose2D pose;
    Point2f MidpointLine = calculateMidpoint(longest_line);
    pose.x = MidpointLine.x;
    pose.y = MidpointLine.y;
    pose.theta = calculateSlope(longest_line);    
    if(pose.x != 0 && pose.y != 0){
      line_pub.publish(pose);
    }

    image_pub_.publish(cv_ptr->toImageMsg());
  }
};

// void reconfigureCb(fast_line_detector::fast_line_detectorConfig &config, uint32_t level) {
//   length_threshold = config.length_threshold;
//   distance_threshold = config.distance_threshold;
//   canny_th1 = config.canny_th1;
//   canny_th2 = config.canny_th2;
//   canny_aperture_size = config.canny_aperture_size;
//   gaussian_kernel_size = config.gaussian_kernel_size;
//   picture_resize_factor = config.picture_resize_factor;
//   PARALLEL_TOLERANCE =  config.PARALLEL_TOLERANCE;
//   DISTANCE_TOLERANCE = config.DISTANCE_TOLERANCE;
// }

double calculateSlope(const Vec4f& line) {
    double x1 = line[0], y1 = line[1], x2 = line[2], y2 = line[3];
    if (x2 - x1 != 0) {
        return (y2 - y1) / (x2 - x1);
    } else {
        return std::numeric_limits<double>::infinity(); // Vertikale Linie
    }
}

bool areParallel(double slope1, double slope2) {
    return std::abs(slope1 - slope2) < PARALLEL_TOLERANCE;
}

double calculateLength(const Vec4f& line) {
    return std::sqrt(std::pow(line[2] - line[0], 2) + std::pow(line[3] - line[1], 2));
}

Point2f calculateMidpoint(const Vec4f& line) {
    return cv::Point2f((line[0] + line[2]) / 2, (line[1] + line[3]) / 2);
}

double pointLineDistance(const Point2f& point, const Vec4f& line) {
    Point2f p1(line[0], line[1]);
    Point2f p2(line[2], line[3]);
    Point2f p = point;

    double norm = cv::norm(p2 - p1);
    double u = ((p.x - p1.x) * (p2.x - p1.x) + (p.y - p1.y) * (p2.y - p1.y)) / (norm * norm);

    u = std::max(0.0, std::min(1.0, u));
    Point2f intersection = p1 + u * (p2 - p1);

    return cv::norm(intersection - p);
}

double lineSegmentDistance(const Vec4f& line1, const Vec4f& line2) {
    Point2f p1(line1[0], line1[1]);
    Point2f p2(line1[2], line1[3]);
    Point2f p3(line2[0], line2[1]);
    Point2f p4(line2[2], line2[3]);

    return std::min({
        pointLineDistance(p1, line2),
        pointLineDistance(p2, line2),
        pointLineDistance(p3, line1),
        pointLineDistance(p4, line1)
    });
}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "fast_line_detector");

  // dynamic_reconfigure::Server<fast_line_detector::fast_line_detectorConfig> server;
  // dynamic_reconfigure::Server<fast_line_detector::fast_line_detectorConfig>::CallbackType f;
  // f = boost::bind(&reconfigureCb, _1, _2);
  // server.setCallback(f);

  ImageConverter ic;
  ros::spin();
  return 0;
}
