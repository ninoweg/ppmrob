#!/usr/bin/python3
import sys
import rospy
import cv2
from cv_bridge import CvBridge, CvBridgeError
from sensor_msgs.msg import Image

class Line_Detector(object):
    def __init__(self):
        self.bridge = CvBridge()
        self.image_sub = rospy.Subscriber("cam0/image_raw", Image, self.camera_callback)

    def camera_callback(self, data):
        try:
            cv_image = self.bridge.imgmsg_to_cv2(data, "bgr8")
        except:
            print(CvBridgeError)
        
        rospy.loginfo(rospy.get_caller_id() + "\n" + "got that good shit")

        fld = cv2.LineSegmentDetector()
        #gray = cv2.cvtColor(cv_image, 'RGB2GRAY')
        fld.detect(cv_image)

        #result_img = fld.drawSegments(cv_image, lines)

        cv2.imshow("Image window", cv_image)
        cv2.waitKey(3)
        

if __name__=="__main__":
    rospy.init_node("line_detector", anonymous=True)
    print("Line detection from cam0/image_raw")
    line_detector = Line_Detector()

    try:
        rospy.spin()
    except KeyboardInterrupt:
        print("Shutting down!")