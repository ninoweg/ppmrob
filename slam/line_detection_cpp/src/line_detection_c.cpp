#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/ximgproc.hpp>
#include <opencv2/imgcodecs.hpp>
#include <nav_msgs/OccupancyGrid.h>
#include <cmath>
#include <vector>
#include <map>

using namespace std;
using namespace cv;
using namespace cv::ximgproc;

struct TapeGradients{
    vector<double> sum_gradients;
    vector<double> angle;
};

struct Gradients{
    vector<double> gradients;
    vector<double> angles;
    vector<int> xCoords;
    vector<int> yCoords;
};

struct HistPeaks{
    double firstPeak;
    double secondPeak;
};

double calculateSlope(const cv::Vec4f& line);
bool areParallel(double slope1, double slope2);
double calculateLength(const cv::Vec4f& line);
cv::Point2f calculateMidpoint(const cv::Vec4f& line);
Gradients computeRadiusGradient(const Mat& image, int radius);
TapeGradients findLineDirection(Gradients grads);
HistPeaks calculateMostFrequentValues(vector<double> values);


static const std::string OPENCV_WINDOW = "Image window";
const double PARALLEL_TOLERANCE = 0.8;
const double TAPE_GRAD_THRESH = 12.0;
const int TAPE_THICKNESS = 11;


class ImageConverter
{
  ros::NodeHandle nh_;
  image_transport::ImageTransport it_;
  image_transport::Subscriber image_sub_;
  image_transport::Publisher image_pub_;
  ros::Publisher map_pub_ = nh_.advertise<nav_msgs::OccupancyGrid>("map", 1, true);

public:
  ImageConverter()
    : it_(nh_)
  {
    // Subscrive to input video feed and publish output video feed
    image_sub_ = it_.subscribe("/cam0/image_raw", 1,
      &ImageConverter::imageCb, this);
    image_pub_ = it_.advertise("/image_converter/output_video", 1);

    cv::namedWindow(OPENCV_WINDOW);
  }

  ~ImageConverter()
  {
    cv::destroyWindow(OPENCV_WINDOW);
  }

  void imageCb(const sensor_msgs::ImageConstPtr& msg)
  {
    cv_bridge::CvImagePtr cv_ptr;
    cv_bridge::CvImagePtr res_cv_ptr;
    try
    {
      cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
    }
    catch (cv_bridge::Exception& e)
    {
      ROS_ERROR("cv_bridge exception: %s", e.what());
      return;
    }

    Gradients results;

    GaussianBlur(cv_ptr->image, cv_ptr->image, Size(3, 3), 0);
    results = computeRadiusGradient(cv_ptr->image, 100);

    // if (!results.angles.empty()) {
    //   for(int i = 0; i < results.angles.size(); i++){
    //     cout  << i << ": " << results.angles[i] << endl;
    //   }
    // }

    TapeGradients directions = findLineDirection(results);
    
    // if (!directions.angle.empty()) {
    //   for(int i = 0; i <= directions.angle.size(); i++){
    //     cout  << i << ": " << directions.angle[i] << " and " << directions.sum_gradients[i] << endl;
    //   }
    // }


    //get top 3 values for being the tape
    static vector<double> values;
    int n;
    int num_directions = 2;
    if(directions.sum_gradients.size() >= num_directions) n = num_directions;
    else                                     n = directions.sum_gradients.size();

    for(int i = 0; i < n; i++){
      auto maxElementIter = max_element(directions.sum_gradients.begin(), directions.sum_gradients.end());
      int maxIndex = distance(directions.sum_gradients.begin(), maxElementIter);
      values.push_back(directions.angle[maxIndex]);
      directions.sum_gradients.erase(directions.sum_gradients.begin() + maxIndex);
      directions.angle.erase(directions.angle.begin() + maxIndex);
    }


    static double value1 = 0;
    static double value2 = 0;
    if(values.size() >= 5*num_directions){
      HistPeaks peaks = calculateMostFrequentValues(values);
      value1 = peaks.firstPeak;
      value2 = peaks.secondPeak;
      cout << "direction1 " << value1 << endl;
      cout << "direction2 " << value2 << endl;
      values.clear();
    }



    Point center(cv_ptr->image.cols / 2, cv_ptr->image.rows / 2);
    int x = static_cast<int>(center.x + 100 * sin(value1* CV_PI/180));
    int y = static_cast<int>(center.y + 100 * -cos(value1* CV_PI/180));
    circle(cv_ptr->image, Point(x, y), 5, Scalar(255, 0, 0), 3);

    x = static_cast<int>(center.x + 100 * sin(value2* CV_PI/180));
    y = static_cast<int>(center.y + 100 * -cos(value2* CV_PI/180));
    circle(cv_ptr->image, Point(x, y), 5, Scalar(255, 0, 0), 3);


    int x_ls = static_cast<int>(center.x + 100 * sin(TAPE_THICKNESS/2* CV_PI/180));
    int y_ls = static_cast<int>(center.y + 100 * -cos(TAPE_THICKNESS/2* CV_PI/180));
    int x_le = static_cast<int>(center.x + 100 * sin(-TAPE_THICKNESS/2* CV_PI/180));
    int y_le = static_cast<int>(center.y + 100 * -cos(-TAPE_THICKNESS/2* CV_PI/180));
    line(cv_ptr->image, Point(x_ls, y_ls), Point(x_le, y_le), Scalar(0, 0, 255), 2);

    // int x = 90;
    // double mittelung0 = (results.gradients[0+x] + results.gradients[1+x] + results.gradients[2+x] + results.gradients[3+x] + results.gradients[4+x])/5;
    // cout << "Gradient at 0: " << mittelung0 << endl;


    // Update GUI Window
    cv::imshow(OPENCV_WINDOW, cv_ptr->image);
    cv::waitKey(3);

    // Output modified video stream
    image_pub_.publish(cv_ptr->toImageMsg());
  }
};

TapeGradients findLineDirection(Gradients grads){
  TapeGradients result;
  int count = 0; // Number of adjacent fields found
  bool long_enough = false;
  int n = 0;
  for (int i = 0; i < grads.gradients.size(); i++) {
    if (grads.gradients[i] < TAPE_GRAD_THRESH) {
      count++;

      if (count >= TAPE_THICKNESS) {
        long_enough = true;
      }
    }
    else if(long_enough == true){
      result.angle.push_back(grads.angles[i-count/2]);

      double grad1 = grads.gradients[i];
      
      if(i-(count+1) < 0) n = i + grads.gradients.size() - (count+1);
      else                n = i-(count+1);
      double grad2 = grads.gradients[n];

      result.sum_gradients.push_back(grad1 + grad2);
      long_enough = false;
      count = 0;      
    }
    else {
      count = 0;
    }

     
  }

  // If we couldn't find n adjacent fields, return an empty vector
  return result;

}

HistPeaks calculateMostFrequentValues(vector<double> values){
    std::map<int, int> histogram; // Map to store the frequency of each range

    HistPeaks peaks;


    static int containerSize = 6;

    for (int i = 0; i < values.size(); ++i) {

        if (values[i] < -180 || values[i] > 180) {
            continue;
        }
        // Calculate the range index
        int rangeIndex = (values[i] + 180) / containerSize;

        // Increment the frequency of the range
        histogram[rangeIndex]++;
    }

    int maxFrequency = 0;
    int mostFrequentRange = 0;
    int maxFrequencyEntry = 0;

    int counter = 0;
    for (const auto& pair : histogram) {
        if (pair.second > maxFrequency) {
            maxFrequency = pair.second;
            mostFrequentRange = pair.first;
            maxFrequencyEntry = counter;
        }
        //cout << "Range: " << (pair.first* containerSize - 180) + containerSize/2 << "    Fq: " << pair.second << endl;
        counter++;
    }

    peaks.firstPeak = (mostFrequentRange * containerSize - 180) + containerSize/2;
    
    //so we found the first peak, now get that stuf out of our data and search for another peak
    //we want to erase about +-20 degree of data around the first peak
    static int containersToErase = 20 / containerSize;
    int n_erase;
    for(int i = -containersToErase*containerSize; i <= containersToErase*containerSize; i = i+containerSize){
      if(mostFrequentRange + i < -180) n_erase = mostFrequentRange + i + 360;
      else if(mostFrequentRange + i > 180) n_erase = mostFrequentRange + i - 360;
      else n_erase = mostFrequentRange + i;
      histogram.erase(n_erase);
      cout << "erased: " << n_erase << endl;
    }

    //

    maxFrequency = 0;
    mostFrequentRange = 0;
    maxFrequencyEntry = 0;
    counter = 0;
    for (const auto& pair : histogram) {
        if (pair.second > maxFrequency) {
            maxFrequency = pair.second;
            mostFrequentRange = pair.first;
            maxFrequencyEntry = counter;
        }
        //cout << "Range: " << (pair.first* containerSize - 180) + containerSize/2 << "    Fq: " << pair.second << endl;
        counter++;
    }

    // Calculate the actual range from the index
    peaks.secondPeak = (mostFrequentRange * containerSize - 180) + containerSize/2;
    return peaks;
}

Gradients computeRadiusGradient(const Mat& image, int radius) {
    Gradients result;
    Mat grayImage;
    cvtColor(image, grayImage, COLOR_BGR2GRAY);

    // Mittelpunkt des Bildes
    Point center(image.cols / 2, image.rows / 2);

    circle(image, center, radius, Scalar(0, 255, 0), 1);

    // Werte für alle Winkel vorberechnen
    vector<double> angles;
    for (int angle = 0; angle < 360; angle++) {
        double radian = angle * CV_PI / 180.0;
        angles.push_back(radian);
    }

    // x- und y-Koordinaten für alle Pixel entlang des Radius vorberechnen
    vector<int> xCoords, yCoords;
    for (int i = 0; i < 360; i++) {
        int x = static_cast<int>(center.x + radius * -sin(angles[i]));
        int y = static_cast<int>(center.y + radius * -cos(angles[i])); //damit 0 Grad vorne ist und nach links positive werte, nach rechts negative werte
        xCoords.push_back(x);
        yCoords.push_back(y);
    }

    // Sobel-Operatoren vorbereiten
    Mat dx, dy;
    Sobel(grayImage, dx, CV_64F, 1, 0);
    Sobel(grayImage, dy, CV_64F, 0, 1);

    // Gradienten und Winkel für alle Punkte berechnen
    vector<double> gradients;
    vector<double> anglesResult;
    for (int i = 0; i < 360; i++) {
        int x = xCoords[i];
        int y = yCoords[i];

        // Überprüfen, ob das Pixel innerhalb des Bildes liegt
        if (x >= 0 && x < image.cols && y >= 0 && y < image.rows) {
            double gradient = sqrt(dx.at<double>(y, x) * dx.at<double>(y, x) + dy.at<double>(y, x) * dy.at<double>(y, x));
            double angle = atan2(y - center.y, x - center.x) * 180.0 / CV_PI;
            result.gradients.push_back(gradient);
            
            double new_angle = angle + 90;
            if(new_angle >= 180) new_angle = new_angle - 360;

            result.angles.push_back(new_angle);
            result.xCoords.push_back(x);
            result.yCoords.push_back(y);
        }
    }
    return result;

}

double calculateSlope(const cv::Vec4f& line) {
    double x1 = line[0], y1 = line[1], x2 = line[2], y2 = line[3];
    if (x2 - x1 != 0) {
        return (y2 - y1) / (x2 - x1);
    } else {
        return std::numeric_limits<double>::infinity(); // Vertikale Linie
    }
}

bool areParallel(double slope1, double slope2) {
    return std::abs(slope1 - slope2) < PARALLEL_TOLERANCE;
}

double calculateLength(const cv::Vec4f& line) {
    return std::sqrt(std::pow(line[2] - line[0], 2) + std::pow(line[3] - line[1], 2));
}

cv::Point2f calculateMidpoint(const cv::Vec4f& line) {
    return cv::Point2f((line[0] + line[2]) / 2, (line[1] + line[3]) / 2);
}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "image_converter");
  ImageConverter ic;
  ros::spin();
  return 0;
}