#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/ximgproc.hpp>
#include <opencv2/imgcodecs.hpp>
#include <nav_msgs/OccupancyGrid.h>
#include <cmath>
#include <vector>
#include <map>

using namespace std;
using namespace cv;
using namespace cv::ximgproc;

struct TapeGradients{
    vector<double> sum_gradients;
    vector<double> angle;
};

struct ColorDistances{
    vector<double> colorDistances;
    vector<double> angles;
};

struct Gradients{
    vector<double> gradients;
    vector<double> angles;
    vector<int> xCoords;
    vector<int> yCoords;
};

struct HistPeaks{
    double firstPeak;
    double secondPeak;
};

double calculateSlope(const cv::Vec4f& line);
bool areParallel(double slope1, double slope2);
double calculateLength(const cv::Vec4f& line);
cv::Point2f calculateMidpoint(const cv::Vec4f& line);
vector<double> findLineDirection(ColorDistances dist);
HistPeaks calculateMostFrequentValues(vector<double> values);


ColorDistances computeColorDistances(const Mat& image, int radius);
void getHSVFromTape(Mat image);


static const std::string OPENCV_WINDOW = "Image window";
const double PARALLEL_TOLERANCE = 0.8;
const double TAPE_GRAD_THRESH = 12.0;
const int TAPE_THICKNESS = 9;
const int COLOR_THRESH = 33;

//Color values for the tape
const int H_TAPE = 75;
const int S_TAPE = 40;
const int V_TAPE = 200;


class ImageConverter
{
  ros::NodeHandle nh_;
  image_transport::ImageTransport it_;
  image_transport::Subscriber image_sub_;
  image_transport::Publisher image_pub_;
  ros::Publisher map_pub_ = nh_.advertise<nav_msgs::OccupancyGrid>("map", 1, true);

public:
  ImageConverter()
    : it_(nh_)
  {
    // Subscrive to input video feed and publish output video feed
    image_sub_ = it_.subscribe("/cam0/image_raw", 1,
      &ImageConverter::imageCb, this);
    image_pub_ = it_.advertise("/image_converter/output_video", 1);

    cv::namedWindow(OPENCV_WINDOW);
  }

  ~ImageConverter()
  {
    cv::destroyWindow(OPENCV_WINDOW);
  }

  void imageCb(const sensor_msgs::ImageConstPtr& msg)
  {
    cv_bridge::CvImagePtr cv_ptr;
    cv_bridge::CvImagePtr res_cv_ptr;
    try
    {
      cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
    }
    catch (cv_bridge::Exception& e)
    {
      ROS_ERROR("cv_bridge exception: %s", e.what());
      return;
    }

    //Gradients results;
    GaussianBlur(cv_ptr->image, cv_ptr->image, Size(3, 3), 0);
    
    //getHSVFromTape(cv_ptr->image);
    
    ColorDistances dist;
    dist = computeColorDistances(cv_ptr->image, 100);

    vector<double> angles = findLineDirection(dist);
    //HistPeaks peaks = calculateMostFrequentValues(angels);
    
    Point center(cv_ptr->image.cols / 2, cv_ptr->image.rows / 2);
    
    for(int i = 0; i < angles.size(); i++){
      int x = static_cast<int>(center.x + 100 * sin(angles[i]* CV_PI/180));
      int y = static_cast<int>(center.y + 100 * -cos(angles[i]* CV_PI/180));
      circle(cv_ptr->image, Point(x, y), 5, Scalar(255, 0, 0), 3);
    
      cout << angles[i] << endl;
    }



    int x_ls = static_cast<int>(center.x + 100 * sin(TAPE_THICKNESS/2* CV_PI/180));
    int y_ls = static_cast<int>(center.y + 100 * -cos(TAPE_THICKNESS/2* CV_PI/180));
    int x_le = static_cast<int>(center.x + 100 * sin(-TAPE_THICKNESS/2* CV_PI/180));
    int y_le = static_cast<int>(center.y + 100 * -cos(-TAPE_THICKNESS/2* CV_PI/180));
    line(cv_ptr->image, Point(x_ls, y_ls), Point(x_le, y_le), Scalar(0, 0, 255), 2);

    // Update GUI Window
    cv::imshow(OPENCV_WINDOW, cv_ptr->image);
    cv::waitKey(3);

    // Output modified video stream
    image_pub_.publish(cv_ptr->toImageMsg());
  }
};

void getHSVFromTape(Mat image){
    Mat HSV;
    Mat RGB=image(Rect(image.cols/2,image.rows/2,1,1)); 

    cvtColor(RGB, HSV,CV_BGR2HSV);
    Vec3b hsv=HSV.at<Vec3b>(0,0);
    int H=hsv.val[0]; //hue
    int S=hsv.val[1]; //saturation
    int V=hsv.val[2]; //value
    static int sum_H = 0;
    static int sum_S = 0;
    static int sum_V = 0;
    
    int n = 100;
    sum_H = sum_H + H; 
    sum_S = sum_S + S;
    sum_V = sum_V + V;
    
    static int i = 0;

    if(i >= n){
        cout << "H:" << sum_H/n << "   S:"<< sum_S/n << "   V:" << sum_V/n << endl;
        i = 0;
        sum_H = 0; 
        sum_S = 0;
        sum_V = 0;
    }

    i++;

    Point center(image.cols / 2, image.rows / 2);
    circle(image, center, 5, Scalar(0, 255, 0), 1);
}


HistPeaks calculateMostFrequentValues(vector<double> values){
    std::map<int, int> histogram; // Map to store the frequency of each range
    HistPeaks peaks;

    static int containerSize = 6;

    for (int i = 0; i < values.size(); ++i) {

        if (values[i] < -180 || values[i] > 180) {
            continue;
        }
        // Calculate the range index
        int rangeIndex = (values[i] + 180) / containerSize;

        // Increment the frequency of the range
        histogram[rangeIndex]++;
    }

    int maxFrequency = 0;
    int mostFrequentRange = 0;
    int maxFrequencyEntry = 0;

    int counter = 0;
    for (const auto& pair : histogram) {
        if (pair.second > maxFrequency) {
            maxFrequency = pair.second;
            mostFrequentRange = pair.first;
            maxFrequencyEntry = counter;
        }
        //cout << "Range: " << (pair.first* containerSize - 180) + containerSize/2 << "    Fq: " << pair.second << endl;
        counter++;
    }

    peaks.firstPeak = (mostFrequentRange * containerSize - 180) + containerSize/2;
    
    //so we found the first peak, now get that stuf out of our data and search for another peak
    //we want to erase about +-20 degree of data around the first peak
    static int containersToErase = 20 / containerSize;
    int n_erase;
    for(int i = -containersToErase*containerSize; i <= containersToErase*containerSize; i = i+containerSize){
      if(mostFrequentRange + i < -180) n_erase = mostFrequentRange + i + 360;
      else if(mostFrequentRange + i > 180) n_erase = mostFrequentRange + i - 360;
      else n_erase = mostFrequentRange + i;
      histogram.erase(n_erase);
      cout << "erased: " << n_erase << endl;
    }

    maxFrequency = 0;
    mostFrequentRange = 0;
    maxFrequencyEntry = 0;
    counter = 0;
    for (const auto& pair : histogram) {
        if (pair.second > maxFrequency) {
            maxFrequency = pair.second;
            mostFrequentRange = pair.first;
            maxFrequencyEntry = counter;
        }
        //cout << "Range: " << (pair.first* containerSize - 180) + containerSize/2 << "    Fq: " << pair.second << endl;
        counter++;
    }

    // Calculate the actual range from the index
    peaks.secondPeak = (mostFrequentRange * containerSize - 180) + containerSize/2;
    return peaks;
}

vector<double> findLineDirection(ColorDistances dist){
  vector<double> angles;
  int count = 0; // Number of adjacent fields found
  bool long_enough = false;
  int n = 0;
  for (int i = 0; i < dist.colorDistances.size(); i++) {
    if (dist.colorDistances[i] < COLOR_THRESH) {
      count++;

      if (count >= TAPE_THICKNESS) {
        long_enough = true;
      }
    }
    else if(long_enough == true){
      angles.push_back(dist.angles[i-count/2]);
      //result.colorDistances.push_back(dist.colorDistances[[i-count/2]])
      // double dist1 = dist.colorDistances[i];
      
      // if(i-(count+1) < 0) n = i + dist.colorDistances.size() - (count+1);
      // else                n = i-(count+1);
      // double dist1 = dist.colorDistances[n];

      // result.sum_gradients.push_back(grad1 + grad2);
      long_enough = false;
      count = 0;      
    }
    else {
      count = 0;
    }    
  }
  return angles;
}


ColorDistances computeColorDistances(const Mat& image, int radius) {
    ColorDistances result;
    Mat hsvImage;
    cvtColor(image, hsvImage,CV_BGR2HSV);

    //einzeichnen des Suchradius
    Point center(image.cols / 2, image.rows / 2);
    circle(image, center, radius, Scalar(0, 255, 0), 1);

    // Werte für alle Winkel vorberechnen
    vector<double> angles;
    for (int angle = 0; angle < 360; angle++) {
        double radian = angle * CV_PI / 180.0;
        angles.push_back(radian);
    }

    // x- und y-Koordinaten für alle Pixel entlang des Radius vorberechnen
    vector<int> xCoords, yCoords;
    for (int i = 0; i < 360; i++) {
        int x = static_cast<int>(center.x + radius * -sin(angles[i]));
        int y = static_cast<int>(center.y + radius * -cos(angles[i])); //damit 0 Grad vorne ist und nach links positive werte, nach rechts negative werte
        xCoords.push_back(x);
        yCoords.push_back(y);
    }

    // Gradienten und Winkel für alle Punkte berechnen
    vector<double> calcColorDistances;
    vector<double> calcAngles;
    for (int i = 0; i < 360; i++) {
        int x = xCoords[i];
        int y = yCoords[i];

        // Überprüfen, ob das Pixel innerhalb des Bildes liegt
        if (x >= 0 && x < hsvImage.cols && y >= 0 && y < hsvImage.rows) {
            
            Vec3b hsv=hsvImage.at<Vec3b>(y,x);
            int H=hsv.val[0]; //hue
            int S=hsv.val[1]; //saturation
            int V=hsv.val[2]; //value
            double dist = abs(H_TAPE - H) + abs(S_TAPE - S);
            double angle = atan2(y - center.y, x - center.x) * 180.0 / CV_PI;
            result.colorDistances.push_back(dist);
            
            double new_angle = angle + 90;
            if(new_angle >= 180) new_angle = new_angle - 360;

            result.angles.push_back(new_angle);
        }
    }
    return result;

}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "line_follow");
  ImageConverter ic;
  ros::spin();
  return 0;
}