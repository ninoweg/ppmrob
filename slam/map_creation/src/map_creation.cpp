#include "ros/ros.h"
#include "std_msgs/String.h"
//#include "init.hpp"

#include <sstream>


int main(int argc, char **argv)
{
    ros::init(argc, argv, "map_creation");
    ros::NodeHandle n;
    ros::Publisher chatter_pub = n.advertise<std_msgs::String>("chatter", 1000);
    ros::Rate loop_rate(10);

    
//   BehaviorTreeFactory factory;

//   RegisterRosSubscriber<DetectionAction>(factory, "Detection", nh);
//   RegisterRosSubscriber<BatteryAction>(factory, "Battery", nh);

//   RegisterRosService<TakeoffLandAction>(factory, "TakeoffLand", nh);
//   RegisterRosService<InitAction>(factory, "Init", nh);
//   RegisterRosService<MoveAction>(factory, "Move", nh);

//   RegisterRosAction<FlyHomeServer>(factory, "FlyHome", nh);

//   auto tree = factory.createTreeFromFile("/catkin_ws/src/behavior_tree/tree/tree.xml");
  
//   PublisherZMQ publisher_zmq(tree);

 //   NodeStatus status = NodeStatus::IDLE;

    int count = 0;
    while( ros::ok())
    {
        std_msgs::String msg;

        std::stringstream ss;
        ss << "hello world " << count;
        msg.data = ss.str();

        ROS_INFO("%s", msg.data.c_str());

        chatter_pub.publish(msg);

        ros::spinOnce();

        loop_rate.sleep();
        ++count;
    }

  return 0;
}